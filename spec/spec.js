const url = 'https://fuji-jaguar.codio.io/';

casper.test.begin('can multiply quantity and price', 5, function suite(test) {
  
  casper.start(url, function() {
    test.assertEquals(this.getTitle(), 'Theta Forum', 'check page title text')
    test.assertVisible('fieldset', 'check form is visible');
    test.assertExists('fieldset > p', 'check form title is present');
    test.assertSelectorHasText('fieldset > p', 'Calculate a bulk book order.', 'check form title text');
    this.fill('form#theForm', {
        'quantity': '10',
        'price': '5'
    });
  });

  casper.thenClick('input#submit', function() {
    var total = this.getFormValues('#theForm').total;
    casper.test.assertEquals(total, '50.00', 'check total is correct');
  });


  casper.run(function() {
    casper.capture('basic-math-1.png');
    test.done();
  });
});